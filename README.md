# project workshop clean arcuitecture

## Primeiros passos

Para começar veja a apresentação [clean_architecture.pdf](clean_arquitecture.pdf).

### Subir o container do app

Na raiz do projeto execute:

```bash
docker-compose up
```

Espere até que algo parecido com isso seja mostrado no seu terminal:

```
clean_arquitecture    | System check identified no issues (0 silenced).
clean_arquitecture    | October 28, 2022 - 18:32:22
clean_arquitecture    | Django version 4.1.2, using settings 'project.settings'
clean_arquitecture    | Starting development server at http://0.0.0.0:8000/
clean_arquitecture    | Quit the server with CONTROL-C.
```

Com isso teremos um projeto do django rodando em http://localhost:8000/

### Popular o banco de dados

1. Criar super usuário

   Execute o seguinte comando e preencha os dados de criação do usuário

   ```bash
   docker exec -it clean_arquitecture python manage.py createsuperuser
   ```

   Se tudo deu certo a seguinte mensagem será mostrada em seu terminal

   ```
   Superuser created successfully.
   ```

2. Django admin

   Em http://localhost:8000/admin faça login com o seu usuário. Com isso, você pode interagir com banco de dados do app por meio do Django admin.
   Em `APP` cadastre um owner e um curriculum vitae de sua preferencia

## Django Admin

Disponível em http://localhost:8000/admin

Interface visual de administrador que permite a interação com o banco do app. Para permitir a interação com uma model,`class ModelName(models.Model)` que é criada no arquivo `models.py`, pelo django admin, basta escrever `admin.site.register(ModelName)` no arquivo `admin.py`. No arquivo `admin.py` também é possível personalizar a interação com model editando, por exemplo, a listagem de itens e o formulário de criação, atulaização e visualização de um item.

## Swagger

Disponível em http://localhost:8000/app/v1/swagger/

Interface visual que lista os endpoints configurados no app. Permite a utilização dos endpoints de forma simplificada, pois utiliza suas credencias do usuário logado no Django Admin.

Sua configuração é feita na view do endpoint por meio de um decorator.

```python
@method_decorator(
    name="post",
    decorator=swagger_auto_schema(
        tags=["curriculum_vitae"], request_body=CurriculumVitaeCreateSerializer
    ),
)
```

No exemplo acima podemos ver a configuração de um post.

## Criação de um app

> **_OBS:_** onde houver app_name é para substituir pelo nome do app que será criado em snake_case. O mesmo vale para as variações como AppName e app-name seguindo o padrão escrito.

1. No terminal vamos executar:

   ```bash
   docker exec -it clean_arquitecture python manage.py startapp app_name
   ```

2. Nas configurações do projeto do django precisamos declarar o novo app asicionando a lista de apps instalados, por padrão no arquivo `settings.py` na pasta do projeto
   ```python
   INSTALLED_APPS = [
       ...,
       "app_name"
   ]
   ```

Assim, um modulo chamado app_name foi criado no projeto com a estrutura padrão do django. E agora precisamos modificar essa estrutura para ajustar ao padrão que adotamos.

> **_OBS:_** cada pasta num projeto em python é um módulo e para seguir o padrão da linguagem temos que criar um arquivo `__init__.py` dentro do módulo. Esse arquivo define como o módulo é iniciado e normalmente ele ficará vazio, mas quando for necessário configurar a inicialiação do módulo mostraremos o que deve ser feito.

1. Criar o arquivo `main.py` dentro da pasta do app

   ```python
   def configure_use_cases():


   def configure():
       configure_use_cases()

   ```

2. Editar o arquivo `apps.py` dentro da pasta do app adicionado o metodo ready

   ```python
   from django.apps import AppConfig


   class AppNameConfig(AppConfig):
       default_auto_field = 'django.db.models.BigAutoField'
       name = 'app_name'

       def ready(self):
           from .main import configure

           configure()
   ```

   - No arquivo `__init__.py` na pasta do app

     ```python
     default_app_config = "app_name.apps.AppConfig"
     ```

3. Criar o arquivo `mixins.py` na pasta do app

4. Criar a pasta `domain` na pasta do app

   1. Criar o arquivo `entities.py` na pasta `domain`

   2. Criar o arquivo `data_access.py` na pasta `domain`

      ```python
      from abc import ABCMeta, abstractmethod
      ```

   3. Criar a pasta `use_cases` na pasta `domain`

      1. Criar o arquivo `base.py` na pasta `use_cases`

      ```python
      from abc import ABCMeta, abstractmethod
      ```

      2. Criar o arquivo `features.py` na pasta `use_cases`

5. Criar a pasta `repositories` na pasta do app

   1. Criar a pasta `parsers` na pasta `repositories`

6. Criar a pasta `api` na pasta do app

   1. Criar a pasta `urls` na pasta `api`

      1. Criar o arquivo `v1_urls.py` na pasta `urls`

         > **_obs:_** esse app_name não é para ser substituido pelo nome do seu app

         ```python
         from django.urls import include, path

         app_name = "v1"
         urlpatterns = []
         ```

      2. Criar o arquivo `base_urls.py` na pasta `urls`

         ```python
         from django.urls import include, path, re_path

         from rest_framework import permissions
         from drf_yasg.views import get_schema_view
         from drf_yasg import openapi

         from app_name.api.urls.v1_urls import urlpatterns as api_v1


         schema_view_v1 = get_schema_view(
             openapi.Info(
                 title="App Name",
                 default_version="v1",
             ),
             public=True,
             permission_classes=(permissions.IsAdminUser,),
             patterns=[
                 path("app-name/v1/", include(api_v1)),
             ],
         )

         urlpatterns = [
             path("v1/", include("app_name.api.urls.v1_urls")),
             re_path(
                 r"^v1/swagger/$",
                 schema_view_v1.with_ui("swagger", cache_timeout=0),
                 name="schema-swagger-ui-v1",
             ),
         ]
         ```

   2. Adicionar nas urls do projeto seu novo app, por padrão, o arquivo `urls.py` na pasta de configurações do projeto

      ```python
      urlpatterns = [
          ...,
          path("app-name/", include("app_name.api.urls.base_urls")),
      ]
      ```

   3. Criar a pasta `v1` na pasta `api`

      1. Criar a pasta `urls` na pasta `v1`

      2. Criar a pasta `views` na pasta `v1`

      3. Criar a pasta `serializers` na pasta `v1`

## Criação de um endpoint seguindo a estratégia bottom-up

Para que isso seja possível é necessário que o app já esteja estruturado na arquitetura.

Esse tutorial é um passo a passo da criação de um endpoint utilizando como referência o endpoint de criação de um curriculum vitae por um usuário. Nesse ponto a model que representa um curriculum vitae já foi criada.

### 1. Criação da url em que o endpoint será acessado

Um endpoint tem um objetivo principal e a url tem que condizer com esse objetivo.
O endpoint de criação do curriculum pode ser acessado em http://localhost:8000/app/v1/curriculum-vitae/create.

A montagem da url aconte em partes:

`/app` foi definido em `project/urls.py` na criação do app
`/v1` foi definido em `app/api/urls/base_urls.py` na criação da v1

Como nosso endpoint tem o foco no curriculum vamos adicionar o path do curriculum ao path do app, caso não exista, todos os endpoints relacionados ao curriculum vão ter essa base.

`/curriculum-vitae` foi definido em `app/api/urls/v1_urls.py`

Essa última parte da url está em um arquivo que conterá todos os paths dos endpoints relacionados ao curriculum

`/create` foi definido em `app/api/v1/urls/url_curriculum_vitae.py`

A url de um endpoint depende de uma view

### 2. Criação da view do endpoint

`app/api/v1/views/views_curriculum_vitae.py`

Na view vamos passar os dados da request para um caso de uso.
Nela é preciso definir as permissões de uso do endpoint, válidar os dados na request, qual caso de uso aquele endpoint utilizará.

A view tem a responsabilidade de passar os dados na request para o caso de uso.

Uma view herda os métodos de uma mixin relacionada com o objetivo do caso de uso. Nesse caso a view `CurriculumVitaeCreateAPIView` utiliza a mixin de `ExecuteUseCaseOnCreateMixin` que implementa não só o método de post como outros metodos úteis para validação utilizando a serializer, obtenção dos dados da request e execução de um caso de uso.

A validação dos dados de entrada usa como base a serializer que foi definida em: `app/api/v1/serializers/serializer_curriculum_vitae.py`
E em `app/api/v1/serializers/__init__.py` precisamos fazer o import do arquivo com as serializers criadas caso não exista.

Na view também definimos por meio de um decorator o swagger para aquele endpoint.

Nesse ponto precisamos criar o caso de uso utilizado.

### 3. Criação do caso de uso que será executado

`app/domain/use_cases/curriculum_vitae.py`

No caso de uso que as regras de negócio da aplicação serão implementadas. Aqui que é definido o que o endpoint irá de fato fazer.

O caso de uso herda a classe `AbstractBaseCurriculumVitaeCreateUseCase` definida em `app/domain/use_cases/base.py`e algumas classes definidas em `app/domain/use_cases/features.py` que auxilia na ligação do caso de uso com as interfaces de output, data access e gatways.

Definimos as interfaces que serão utilizadas, no nosso caso apenas as de data access que foi declarada em `app/domain/data_access.py` e output response que foi declarada em `app/domain/use_cases/base.py`

As interfaces utilizadas nos casos de uso são abstratas, pois o caso se uso não deve dependender de nada além das entidades. Dessa maneira vamos precisar fazer uma injeção de dependência, como a classe abstrata apenas declara os métodos, vamos precisar de quem implementa de fato os métodos. No nosso caso, quem faz o acesso aos dados é uma repository, então vamos configurar fazer que os métodos imlementados na repository sejam usados quando estivermos usando o data_access.

O arquivo responsável pelas injeções de dependência é o `app/main.py`

No metodo `__init__` vamos pegar os dados que foram passado pela view

No metodo `execute` vamos implementar o que o caso de uso deve fazer, no caso da criação de um currilum. Assim vamos usar o data_access para fazer essa criação o método utilizado será implementado na respository e retornará uma entidade para que a gente possa usar no caso de uso.

No metodo `build_output` vamos montar o conteúdo da response

Em `app/domain/use_cases/__init__.py` precisamos fazer o import do arquivo com os casos de uso criados caso não exista.

### 4. Criação da repository para acesso ao banco

`app/repositories/curriculum_vitae.py`

Na repository vamos implementar os metodos declarados pelo data access. Aqui que acontece a interação com o banco por meio das models do django.

No caso, para criar um curriculum pegamos um owner no banco a partir do id passado e e criamos um curriculo passando o owner e o resume. Após essas operções salvamos a model no banco.

A repository retornará uma entidade, então precisamos fazer um parse de model para entity em `app/repositories/parsers/curriculum_vitae.py`

Em `app/repositories/__init__.py` precisamos fazer o import do arquivo com as respositories criadas caso não exista.
Em `app/repositories/parsers/__init__.py` precisamos fazer o import do arquivo com os parses criados caso não exista.

### 5. Criação das entidades

`app/domain/entities.py`

A entidade é o nível mais alto da aplicação e não tem dependências.
Aqui é definido as regras de nengócio da empresa, uma representação dos objetos do mundo real.

Não deve ser entendida como apenas uma representação de uma model sem ligação com o framework, pois nela podem estar métodos que definem o funcionameto daquela entidade.
