from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from drf_yasg.utils import swagger_auto_schema
from django.utils.decorators import method_decorator

from app.api.v1.serializers import CurriculumVitaeCreateSerializer
from app.api_output import DjangoApiOutput
from app.domain.use_cases.curriculum_vitae import CurriculumVitaeCreateUseCase
from app.mixins import ExecuteUseCaseOnCreateMixin

@method_decorator(
    name="post",
    decorator=swagger_auto_schema(
        tags=["curriculum_vitae"], request_body=CurriculumVitaeCreateSerializer
    ),
)
class CurriculumVitaeCreateAPIView(APIView, ExecuteUseCaseOnCreateMixin):
    permission_classes = (IsAuthenticated,)
    serializer = CurriculumVitaeCreateSerializer
    use_case = CurriculumVitaeCreateUseCase
    use_case_output = DjangoApiOutput

