from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from drf_yasg.utils import swagger_auto_schema
from django.utils.decorators import method_decorator

from app.api.v1.serializers import OwnerRetrieveSerializer
from app.api_output import DjangoApiOutput
from app.domain.use_cases.owner import OwnerRetrieveUseCase
from app.mixins import ExecuteUseCaseOnGetMixin

@method_decorator(
    name="get",
    decorator=swagger_auto_schema(
        tags=["owner"], query_serializer=OwnerRetrieveSerializer
    ),
)
class OwnerRetrieveAPIView(APIView, ExecuteUseCaseOnGetMixin):
    permission_classes = (IsAuthenticated,)
    query_serializer = OwnerRetrieveSerializer
    use_case = OwnerRetrieveUseCase
    use_case_output = DjangoApiOutput

    def get_use_case_kwargs(self, request, *args, **kwargs):
        return {
            "id": kwargs.get("id", None),
        }