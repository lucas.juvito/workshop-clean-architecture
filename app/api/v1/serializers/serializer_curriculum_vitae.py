from rest_framework import serializers


class CurriculumVitaeCreateSerializer(serializers.Serializer):
    owner = serializers.SerializerMethodField()
    resume = serializers.CharField(required=True, allow_null=False)

    def get_owner(self, obj):
        request = self.context["request"]

        return {"id": request.user.owner.id}
