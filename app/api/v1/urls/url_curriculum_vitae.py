from django.urls import path

from app.api.v1.views import views_curriculum_vitae as views

urlpatterns = [
    path(
        "create",
        views.CurriculumVitaeCreateAPIView.as_view(),
        name="cv_create"
    ),
]
