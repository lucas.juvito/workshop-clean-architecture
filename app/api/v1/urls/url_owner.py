from django.urls import path

from app.api.v1.views import views_owner as views

urlpatterns = [
    path(
        "<int:id>",
        views.OwnerRetrieveAPIView.as_view(),
        name="owner_retrieve"
    ),
]
