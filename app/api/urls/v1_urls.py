from django.urls import include, path


app_name = "v1"
urlpatterns = [
    path("owner/", include("app.api.v1.urls.url_owner")),
    path("curriculum-vitae/", include("app.api.v1.urls.url_curriculum_vitae")),
]
