from django.urls import include, path, re_path

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from app.api.urls.v1_urls import urlpatterns as api_v1


schema_view_v1 = get_schema_view(
    openapi.Info(
        title="App 😎",
        default_version="v1",
    ),
    public=True,
    permission_classes=(permissions.IsAdminUser,),
    patterns=[
        path("app/v1/", include(api_v1)),
    ],
)

urlpatterns = [
    path("v1/", include("app.api.urls.v1_urls")),
    re_path(
        r"^v1/swagger/$",
        schema_view_v1.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui-v1",
    ),
]
