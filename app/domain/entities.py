from datetime import date, datetime


class User:
    def __init__(self,
        id = None,
        username = None,
        email = None,
    ) -> None:
        self.id = id
        self.username = username
        self.email = email


class Owner:
    def __init__(self, 
        id: int = None,
        user: User = None,
        name: str = None,
        birthday: date = None
    ) -> None:
        self.id = id
        self.user = user 
        self.name = name 
        self.birthday = birthday 


class CurriculumVitae:
    def __init__(self, 
        id: int = None, 
        owner: Owner = None,
        resume: str = None,
        created_at: datetime = None,
        updated_at: datetime = None
    ) -> None:
        self.id = id
        self.owner = owner
        self.resume = resume
        self.created_at = created_at
        self.updated_at = updated_at