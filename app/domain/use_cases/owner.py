from .base import (
    AbstractBaseOutput,
    AbstractBaseOwnerRetrieveUseCase,
)

from .features import (
    GetDataAccessUseCaseMixin,
    ValidateDataAccessUseCaseMixin,
    GetOutputResponseUseCaseMixin,
    ValidateOutputResponseUseCaseMixin,
)

from app.domain.data_access import AbstractBaseOwnerRetrieveDataAccess

from django.contrib.auth.models import User


class OwnerRetrieveUseCase(
    AbstractBaseOwnerRetrieveUseCase,
    GetOutputResponseUseCaseMixin,
    ValidateOutputResponseUseCaseMixin,
    GetDataAccessUseCaseMixin,
    ValidateDataAccessUseCaseMixin,
):
    data_access: AbstractBaseOwnerRetrieveDataAccess = None
    output_response: AbstractBaseOutput = None

    def __init__(self, id):
        super().__init__()
        self.id = id

    def execute(self) -> AbstractBaseOutput:
        self.result = self.get_data_access().get_owner(self.id)

        return self.build_output()

    def build_output(self):
        self.output = self.get_output_response()
        data = {
            "id": self.result.id,
            "name": self.result.name,
            "birthday": self.result.birthday,
            "user": {
                "id": self.result.user.id,
                "username": self.result.user.username,
                "email": self.result.user.email,
            },
        }
        self.output.data = data

        return self.output
