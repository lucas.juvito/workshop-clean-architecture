from .base import (
    AbstractBaseOutput,
    AbstractBaseCurriculumVitaeCreateUseCase,
)

from .features import (
    GetDataAccessUseCaseMixin,
    ValidateDataAccessUseCaseMixin,
    GetOutputResponseUseCaseMixin,
    ValidateOutputResponseUseCaseMixin,
)

from app.domain.data_access import AbstractBaseCurriculumVitaeCreateDataAccess


class CurriculumVitaeCreateUseCase(
    AbstractBaseCurriculumVitaeCreateUseCase,
    GetOutputResponseUseCaseMixin,
    ValidateOutputResponseUseCaseMixin,
    GetDataAccessUseCaseMixin,
    ValidateDataAccessUseCaseMixin,
):
    data_access: AbstractBaseCurriculumVitaeCreateDataAccess = None
    output_response: AbstractBaseOutput = None

    def __init__(self, data):
        super().__init__()
        self.owner = data.get("owner", None).get("id", None)
        self.resume = data.get("resume", None)

    def execute(self) -> AbstractBaseOutput:
        self.result = self.get_data_access().save_curriculum_vitae(
            self.owner,
            self.resume
        )

        return self.build_output()

    def build_output(self):
        self.output = self.get_output_response()
        data = {
            "id": self.result.id,
            "owner": self.result.owner.name,
            "created_at": self.result.created_at,
            "updated_at": self.result.updated_at,     
        }
        self.output.data = data

        return self.output