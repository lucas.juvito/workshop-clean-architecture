from abc import ABCMeta, abstractmethod


class AbstractBaseOutput(metaclass=ABCMeta):
    """
    Base class for use cases output.
    """

    @property
    @abstractmethod
    def data(self) -> dict:
        pass

    @data.setter
    @abstractmethod
    def data(self, value: dict):
        pass


class AbstractBaseCurriculumVitaeCreateUseCase(metaclass=ABCMeta):
    """
    Base class for create curriculum vitae use case.
    """

    @abstractmethod
    def execute(self) -> AbstractBaseOutput:
        pass

class AbstractBaseOwnerRetrieveUseCase(metaclass=ABCMeta):
    """
    Base class for retireve owner use case.
    """

    @abstractmethod
    def execute(self) -> AbstractBaseOutput:
        pass