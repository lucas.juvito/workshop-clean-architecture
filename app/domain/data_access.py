from abc import ABCMeta, abstractmethod

from .entities import CurriculumVitae, Owner

class AbstractBaseCurriculumVitaeCreateDataAccess(metaclass=ABCMeta):
    @abstractmethod
    def save_curriculum_vitae(self, owner, resume) -> CurriculumVitae:
        pass

class AbstractBaseOwnerRetrieveDataAccess(metaclass=ABCMeta):
    @abstractmethod
    def get_owner(self, id) -> Owner:
        pass