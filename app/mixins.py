from functools import reduce

from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework.pagination import LimitOffsetPagination

User = get_user_model()

class ExecuteUseCaseOnGetMixin(object):
    use_case = None
    use_case_retrieve = None
    use_case_output = None
    use_case_output_retrieve = None
    image_fields = None
    query_serializer = None

    def get(self, request, *args, **kwargs):
        try:
            uc = self.execute_use_case_retrieve(request, *args, **kwargs)
            response = (
                uc.get_response() 
                if hasattr(uc, "get_response") 
                else Response(uc.data, status=status.HTTP_200_OK)
            )
            if self.image_fields:
                response = self.apply_domain_host_in_image_fields(
                    request, response, *args, **kwargs
                )
            if response.data is None:
                return Response(
                    {"detail": "object not found"},
                    status=status.HTTP_404_NOT_FOUND,
                )
            return response
        except self.Http400Error as e:
            print(e)
            return Response(e.args[0], status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            if hasattr(e, "code") and e.code < 500:
                return Response({"detail": e.args[0]}, status=e.code)
            raise

    def execute_use_case_retrieve(self, request, *args, **kwargs):
        use_case_class = self.get_use_case_retrieve()
        output_response = self.get_use_case_output_retrieve()
        if output_response:
            use_case_class.output_response = output_response
        uc = use_case_class(
            **self.get_use_case_kwargs_retrieve(request, *args, **kwargs)
        )
        return uc.execute()

    def get_use_case_retrieve(self):
        return self.use_case_retrieve if self.use_case_retrieve else self.use_case

    def get_use_case_kwargs_retrieve(self, request, *args, **kwargs):
        data = self.get_use_case_kwargs(request, *args, **kwargs)
        if self.query_serializer:
            serializer = self.query_serializer(data=data)
            if not serializer.is_valid():
                raise self.Http400Error(serializer.errors)
            data = {**data, **serializer.validated_data}
        return data

    def get_use_case_kwargs(self, request, *args, **kwargs):
        return {}

    def get_use_case_output_retrieve(self):
        return (
            self.use_case_output_retrieve
            if self.use_case_output_retrieve
            else self.use_case_output
        )

    def apply_domain_host_in_image_fields(self, request, response, *args, **kwargs):
        def nested_update(output_response):
            if isinstance(output_response, dict):
                for key, value in output_response.items():
                    if isinstance(value, (dict, list)):
                        nested_update(value)
                    elif key in self.image_fields:
                        output_response[key] = (
                            f"{request.scheme}://{request.get_host()}{value}"
                            if value
                            else None
                        )
            elif isinstance(output_response, list):
                for item in output_response:
                    nested_update(item)

        nested_update(response.data)
        return response

    class Http400Error(Exception):
        pass


class ExecuteUseCaseOnUpdateMixin(object):
    use_case = None
    use_case_update = None
    use_case_output = None
    use_case_output_update = None
    serializer = None
    image_fields = None

    def put(self, request, *args, **kwargs):
        serializer = self.get_serializer(request)
        return self.update_data(request, serializer, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        serializer = self.get_serializer(request, partial=True)
        return self.update_data(request, serializer, *args, **kwargs)

    def update_data(self, request, serializer, *args, **kwargs):
        try:
            if serializer.is_valid():
                uc = self.execute_use_case_update(
                    request, serializer.validated_data, *args, **kwargs
                )
                response = uc.get_response()
                if self.image_fields:
                    response = self.apply_domain_host_in_image_fields(
                        request, response, *args, **kwargs
                    )
                if response.data is None:
                    return Response(
                        {"detail": "object not found"},
                        status=status.HTTP_404_NOT_FOUND,
                    )
                return response
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST,
            )
        except Exception as e:
            if hasattr(e, "code") and type(e.code) == int and e.code < 500:
                return Response({"detail": e.args[0]}, status=e.code)
            raise

    def get_serializer(self, request, partial=False):
        serializer = self.serializer(
            data=request.data,
            context={
                "request": request,
            },
            partial=partial,
        )
        return serializer

    def execute_use_case_update(self, request, data, *args, **kwargs):
        use_case_class = self.get_use_case_update()
        use_case_class.output_response = self.get_use_case_output_update()
        uc = use_case_class(
            **self.get_use_case_kwargs_update(request, data, *args, **kwargs)
        )
        return uc.execute()

    def get_use_case_update(self):
        return self.use_case_update if self.use_case_update else self.use_case

    def get_use_case_kwargs_update(self, request, data, *args, **kwargs):
        return self.get_use_case_kwargs(request, data, *args, **kwargs)

    def get_use_case_kwargs(self, request, data, *args, **kwargs):
        return {}

    def get_use_case_output_update(self):
        return (
            self.use_case_output_update
            if self.use_case_output_update
            else self.use_case_output
        )

    def apply_domain_host_in_image_fields(self, request, response, *args, **kwargs):
        def nested_update(output_response):
            if isinstance(output_response, dict):
                for key, value in output_response.items():
                    if isinstance(value, (dict, list)):
                        nested_update(value)
                    elif key in self.image_fields:
                        output_response[key] = (
                            f"{request.scheme}://{request.get_host()}{value}"
                            if value
                            else None
                        )
            elif isinstance(output_response, list):
                for item in output_response:
                    nested_update(item)

        nested_update(response.data)
        return response

    class Http400Error(Exception):
        pass


class MixinListAPIView(GenericAPIView):
    pagination_class = LimitOffsetPagination

    def get_queryset(self, *args, **kwargs):
        pass

    def pre_get(self, request, *args, **kwargs):
        self._build_pagination(request)

    def get(self, request, *args, **kwargs):
        self.pre_get(request, *args, **kwargs)
        count, data = self.execute(
            request, self.paginator.limit, self.paginator.offset, *args, **kwargs
        )
        self._build_pagination(request, count)
        response = self.get_paginated_response(data)

        return response

    def _build_pagination(self, request, count=0):
        self.paginator.count = count
        self.paginator.request = request
        self.paginator.offset = self.paginator.get_offset(request)
        self.paginator.limit = self.paginator.get_limit(request)

        if (
            self.paginator.count > self.paginator.limit
            and self.paginator.template is not None
        ):
            self.display_page_controls = True

    def get_context(self):
        return self.get_serializer_context()

    def execute(self, request, *args, **kwargs):
        raise NotImplementedError()


class ExecuteUseCaseOnDestroyMixin(object):
    use_case = None
    use_case_destroy = None
    use_case_output = None
    use_case_output_destroy = None
    image_fields = None

    def delete(self, request, *args, **kwargs):
        try:
            uc = self.execute_use_case_destroy(request, *args, **kwargs)
            response = uc.get_response()
            if response.data is None:
                return Response(
                    {"detail": "object not found"},
                    status=status.HTTP_404_NOT_FOUND,
                )
            return response
        except self.Http400Error as e:
            print(e)
            return Response(e.args[0], status=e.code)
        except Exception as e:
            if hasattr(e, "code") and e.code < 500:
                return Response({"detail": e.args[0]}, status=e.code)
            raise

    def execute_use_case_destroy(self, request, *args, **kwargs):
        use_case_class = self.get_use_case_destroy()
        use_case_class.output_response = self.get_use_case_output_destroy()
        uc = use_case_class(
            **self.get_use_case_kwargs_destroy(request, *args, **kwargs)
        )
        return uc.execute()

    def get_use_case_destroy(self):
        return self.use_case_destroy if self.use_case_destroy else self.use_case

    def get_use_case_kwargs_destroy(self, request, *args, **kwargs):
        return self.get_use_case_kwargs(request, *args, **kwargs)

    def get_use_case_kwargs(self, request, *args, **kwargs):
        return {}

    def get_use_case_output_destroy(self):
        return (
            self.use_case_output_destroy
            if self.use_case_output_destroy
            else self.use_case_output
        )

    class Http400Error(Exception):
        pass


class ExecuteUseCaseOnPutMixin(object):
    use_case = None
    use_case_output = None
    image_fields = None

    def put(self, request, *args, **kwargs):
        self.use_case.output_response = self.use_case_output
        try:
            uc = self.execute(request, *args, **kwargs)
            response = uc.get_response()
            if response.data is None:
                return Response(
                    {"detail": "object not found"},
                    status=status.HTTP_404_NOT_FOUND,
                )
            return response
        except self.Http400Error as e:
            print(e)
            return Response(e.args[0], status=e.code)
        except Exception as e:
            if hasattr(e, "code") and e.code < 500:
                return Response({"detail": e.args[0]}, status=e.code)
            raise

    def execute(self, request, *args, **kwargs):
        uc = self.use_case(**self.get_use_case_kwargs(request, *args, **kwargs))
        return uc.execute()

    def get_use_case_kwargs(self, request, *args, **kwargs):
        return {}

    class Http400Error(Exception):
        pass


class ExecuteUseCaseOnCreateMixin(object):
    use_case = None
    use_case_create = None
    use_case_output = None
    use_case_output_create = None
    image_fields = None
    serializer = None
    serializer_create = None
    serializer_instance = None

    def post(self, request, *args, **kwargs):
        self.serializer_instance = self.get_serializer_create()(
            data=request.data, context={"request": request}
        )
        if self.serializer_instance.is_valid():
            try:
                uc = self.execute_use_case_create(
                    request, data=self.serializer_instance.data, *args, **kwargs
                )
                response = (
                    uc.get_response() 
                    if hasattr(uc, "get_response") 
                    else Response(uc.data, status=status.HTTP_200_OK)
                )
                if response.data is None:
                    return Response(
                        {"detail": "object not found"},
                        status=status.HTTP_404_NOT_FOUND,
                    )
                return response
            except self.Http400Error as e:
                print(e)
                return Response(e.args[0], status=e.code)
            except Exception as e:
                response = self.catch_error(e)

                if isinstance(response, Response):
                    return response
                raise

        return Response(self.serializer_instance.errors, status=status.HTTP_400_BAD_REQUEST)

    def execute_use_case_create(self, request, data, *args, **kwargs):
        use_case_class = self.get_use_case_create()
        use_case_class.output_response = self.get_use_case_output_create()
        uc = use_case_class(
            data=data, **self.get_use_case_kwargs_create(request, *args, **kwargs)
        )
        return uc.execute()

    def get_use_case_create(self):
        return self.use_case_create if self.use_case_create else self.use_case

    def get_use_case_kwargs_create(self, request, *args, **kwargs):
        return self.get_use_case_kwargs(request, *args, **kwargs)

    def get_use_case_kwargs(self, request, *args, **kwargs):
        return {}

    def get_use_case_output_create(self):
        return (
            self.use_case_output_create
            if self.use_case_output_create
            else self.use_case_output
        )

    def get_serializer_create(self):
        return self.serializer_create if self.serializer_create else self.serializer

    def catch_error(self, error: Exception):
        if hasattr(error, "code") and error.code < 500:
            return Response({"detail": error.args[0]}, status=error.code)
        raise

    class Http400Error(Exception):
        pass
