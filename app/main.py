from .domain import use_cases

from app.repositories import CurriculumVitaeCreateRepository, OwnerRetrieveRepository


def configure_use_cases():
    use_cases.CurriculumVitaeCreateUseCase.data_access = CurriculumVitaeCreateRepository

    use_cases.OwnerRetrieveUseCase.data_access = OwnerRetrieveRepository


def configure():
    configure_use_cases()
