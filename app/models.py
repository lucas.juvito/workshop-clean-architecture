from django.db import models
from django.contrib.auth.models import User


class Owner(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, blank=False, null=False)
    birthday = models.DateField(
        auto_now=False, auto_now_add=False, blank=True, null=True
    )

    def __str__(self):
        return self.name


class CurriculumVitae(models.Model):
    owner = models.ForeignKey(
        Owner,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        related_name="curricula_vitae",
    )
    resume = models.TextField(blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name_plural = "curricula vitae"
