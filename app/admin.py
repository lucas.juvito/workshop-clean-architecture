from django.contrib import admin

from app.models import CurriculumVitae, Owner

# Register your models here.
class OwnerAdmin(admin.ModelAdmin):
    list_display = ("id","user","name","birthday")

class CurriculumVitaeAdmin(admin.ModelAdmin):
    list_display = ("id","owner","get_resume", "created_at", "updated_at")
    list_filter = ("owner","created_at")

    def get_resume(self, obj):
        return (obj.resume[:30] + "...") if len(obj.resume) > 25 else obj.resume

    get_resume.short_description = "resume"


admin.site.register(Owner, OwnerAdmin)
admin.site.register(CurriculumVitae, CurriculumVitaeAdmin)