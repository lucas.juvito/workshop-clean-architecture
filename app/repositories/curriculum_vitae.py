from app.domain.data_access import AbstractBaseCurriculumVitaeCreateDataAccess
from django.db import transaction

from app.domain.entities import CurriculumVitae
from app.models import CurriculumVitae as CurriculumVitaeModel, Owner as OwnerModel
from app.repositories.parsers import model_curriculum_vitae_to_curriculum_vitae


class CurriculumVitaeCreateRepository(AbstractBaseCurriculumVitaeCreateDataAccess):
    def save_curriculum_vitae(self, owner, resume) -> CurriculumVitae:
        with transaction.atomic():
            owner_db = OwnerModel.objects.get(id=owner)
            cv_db = CurriculumVitaeModel(owner=owner_db, resume=resume)
            cv_db.save()

            return model_curriculum_vitae_to_curriculum_vitae(cv_db)