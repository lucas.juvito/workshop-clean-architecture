from app.domain.entities import Owner, User
from app.models import Owner as OwnerModel
from django.contrib.auth.models import User as UserModel



def model_user_to_user(user: UserModel) -> User:
    data = {
        "id": user.id,
        "username": user.username,
        "email": user.email,
    }
    return User(**data)

def model_owner_to_owner(owner: OwnerModel) -> Owner:
    user = model_user_to_user(owner.user)

    data = {
        "id": owner.id,
        "user": user,
        "name": owner.name,
        "birthday": owner.birthday
    }
    return Owner(**data)