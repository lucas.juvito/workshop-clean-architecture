from app.domain.entities import CurriculumVitae
from app.models import CurriculumVitae as CurriculumVitaeModel
from app.repositories.parsers import model_owner_to_owner


def model_curriculum_vitae_to_curriculum_vitae(cv: CurriculumVitaeModel) -> CurriculumVitae:
    owner = model_owner_to_owner(cv.owner)
    
    data = {
        "id": cv.id,
        "owner": owner,
        "created_at": cv.created_at,
        "updated_at": cv.updated_at,
    }
    return CurriculumVitae(**data)