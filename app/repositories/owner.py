from app.domain.data_access import AbstractBaseOwnerRetrieveDataAccess

from app.domain.entities import CurriculumVitae
from app.models import Owner as OwnerModel
from app.repositories.parsers import model_owner_to_owner


class OwnerRetrieveRepository(AbstractBaseOwnerRetrieveDataAccess):
    def get_owner(self, id) -> CurriculumVitae:
        return model_owner_to_owner(OwnerModel.objects.get(id=id))