from rest_framework.response import Response
from rest_framework import status

from typing import Dict

from .domain.use_cases import AbstractBaseOutput


class DjangoApiOutput(AbstractBaseOutput):
    """
    Class responsible for list scheduled order cart output.
    """

    def __init__(self, *args, **kwargs):
        self._data = None

    @property
    def data(self) -> Dict:
        return self._data

    @data.setter
    def data(self, value: Dict):
        self._data = value

    def get_response(self):
        return Response(self.data, status=status.HTTP_200_OK)
